<?php defined('C5_EXECUTE') or die(_('Access Denied.'));

class DashboardBuilderController extends DashboardBaseController {
	
	public function view() {
		$this->redirect('/dashboard/builder/list');
	}
	
}