<?php defined('C5_EXECUTE') or die(_('Access Denied.'));

class Genesis extends Object {

	protected $id;
	protected $name;
	protected $class;
	protected $handle;
	static $table = 'Geneses';
	
	public static function add($name) {

		// sanitize name convention
		$name = Object::camelcase($name); 

		// this will create the required filesystem structure and insert the tables for the new genesis
		$creator = new Creator($name);
		$creator->create();

		// now we will add an entry into our geneses table for the newly created genesis
		$table = self::$table;
		$db = Loader::db();
		$db->Execute("INSERT INTO {$table} (name, handle) VALUES (?, ?)", array($creator->name, $creator->handle));
		$id = $db->Insert_ID();
		$genesis = self::getById($id);

		// fire an event for the genesis creation
		Events::fire('on_genesis_add', $genesis);

		// register our new attribute key category
		$category = AttributeKeyCategory::add($genesis->getHandle(), AttributeKeyCategory::ASET_ALLOW_SINGLE, Package::getByHandle('builder'));

		return $genesis;
	}

	public function getName() {
		return $this->name;
	}
	
	public function getClass() {
		return $this->class;
	}

	public function getClassName() {
		return $this->getClass();
	}
	
	public function getId() {
		return $this->id;
	}

	public function getHandle() {
		return $this->handle;
		//return self::uncamelcase($this->getName());
	}
	
	public function delete() {
		$id = $this->getId();
		if ($id < 1) {
			return false;
		}

		$return = Events::fire('on_genesis_delete', $this);
		if ($return < 0) {
			return false;
		}

		$table = self::$table;
		$db = Loader::db();
		$db->Execute("DELETE FROM {$table} WHERE id = ?", $id);

		// delete all sub-tables & unlink generated files
		$creator = new Creator($this->getName());
		$creator->destroy();

		// remove attribute key category
		$category = AttributeKeyCategory::getByHandle($this->getHandle());
		if (is_object($category)) {
			$category->delete();
		}
	}

	private function populate($value, $where) {
		$table = self::$table;
		$db = Loader::db();
		$row = $db->getOne("SELECT * FROM {$table} WHERE ? = ?", array($where, $value));
        $this->setPropertiesFromArray($row);
	}

	public static function getById($id) {
		$table = self::$table;
		$db = Loader::db();
		$row = $db->getRow("SELECT id, name, handle FROM {$table} WHERE id = ?", $id);
		if (is_array($row)) {
			$genesis = new Genesis();
			$genesis->setPropertiesFromArray($row);
		}
        return $genesis;
    }

	public static function getByName($name) {
		$table = self::$table;
		$db = Loader::db();
		$row = $db->getRow("SELECT id, name, handle FROM {$table} WHERE name = ?", $name);
		if (is_array($row)) {
			$genesis = new Genesis();
			$genesis->setPropertiesFromArray($row);
		}
        return $genesis;
    }


	// public static function getById($id) {
	// 	$genesis = new self;
	// 	$genesis->populate($id, 'id');
	// 	return $genesis;
	// }

	// public static function getByName($name) {
	// 	$genesis = new self;
	// 	$genesis->populate($name, 'name');
	// 	return $genesis;
	// }

	public static function getAll() {
		$table = self::$table;
		$db = Loader::db();
		$rows = $db->getAll("SELECT id FROM {$table}");
		foreach ($rows as $row) {
			$geneses[] = Genesis::getById($row['id']);
		}
		return $geneses;
	}

}

