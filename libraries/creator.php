<?php defined('C5_EXECUTE') or die(_('Access Denied.'));

/**
 * Class that handles generation of the required files to support the new Genesis
 */
class Creator extends Object {

	public $class;
	public $handle;
	protected $output_path;
	protected $template_path;
	protected $chmod = 0777;
	protected $token = '[[[GENESIS]]]';

	public function __construct($class) {
		$this->setProperties($class);
		$this->output_path = DIR_BASE . '/' . DIRNAME_PACKAGES . '/builder/' . DIRNAME_MODELS . '/';
		$this->template_path = DIR_BASE . '/' . DIRNAME_PACKAGES . '/builder/templates/';
	}

	public function create($is_virtual = true) {
		$errors = $this->isBlocked();
		if ($errors === false) {

			$this->createOutputDirectory();

			$model = $this->generateContents('model.php');
			$this->outputFile($this->getModelPath(), $model);

			$attribute_category = $this->generateContents('attribute_category.php');
			$this->outputFile($this->getAttributeCategoryPath(), $attribute_category);

			$db = $this->generateContents('db.xml');
			$this->outputFile($this->getDbPath(), $db);
			Package::installDB($this->getDbPath());

			return $this;

		} else {
			var_dump($errors);
			throw new Exception(t('Environment is not prepared correctly: ') . implode(', ', $errors));
		}
	}

	protected function setProperties($class) {
		$this->class = self::camelcase($class);
		$this->handle = self::uncamelcase($this->class);
	}

	public function destroy() {
		unlink($this->getModelPath());
		unlink($this->getAttributeCategoryPath());
		unlink($this->getDbPath());
		rmdir ($this->getOutputPath());
		$db = Loader::db();
		$db->Execute("DROP TABLE IF EXISTS {$this->class}s");
		$db->Execute("DROP TABLE IF EXISTS {$this->class}AttributeKeys");
		$db->Execute("DROP TABLE IF EXISTS {$this->class}AttributeValues");
	}

	// stub for when we integrate some system checks 
	protected function isBlocked() {
		$errors = array();
		if (class_exists($this->class)) {
			$errors[] = t('a class with the name "%s" already exists.', $this->class);
		}
		if (!is_writable($this->output_path)) {
			$errors[] = t('Builder directory is not writable.');
		}
		if (file_exists($this->output_path . $this->handle)) {
			$errors[] = t('a genesis with the name "%s" already exists.', $this->class);
		}
		return (count($errors)) ? $errors : false;
	}

	protected function generateContents($filename) {
		$template = file_get_contents($this->template_path . '_' . $filename);
		$template = str_replace($this->token, $this->class, $template);
		return $template;
	}

	protected function outputFile($to_path, $contents) {
		file_put_contents($to_path, $contents);
		$this->setFilePermission($to_path);
	}

	protected function setFilePermission($filepath) {
		if ($this->chmod !== false) {
			@chmod($filepath, $this->chmod);
		}		
	}

	public function setChmod($chmod) {
		if (is_int($chmod)) {
			$this->chmod = $chmod;
		}
	}

	protected function createOutputDirectory() {
		mkdir($this->getOutputPath());
	}

	public function getModelPath() {
		return $this->getOutputPath() . '/model.php';
	}

	public function getAttributeCategoryPath() {
		return $this->output_path . 'attribute/categories/' . $this->handle . '.php';
	}

	public function getDbPath() {
		return $this->getOutputPath() . '/db.xml';
	}

	public function getOutputPath() {
		return $this->output_path . $this->handle;
	}

}