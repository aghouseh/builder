<?php defined('C5_EXECUTE') or die(_('Access Denied.'));

class Prime {

	protected $data = array(
		'table' => array(
			'values'       => 'AttributeValues',
			'search_index' => 'SearchIndexAttributes'
		),
		'class_name' => array(
			'key'   => 'AttributeKey',
			'value' => 'AttributeValue'
		)
	);

	public static function add($name) {}

	public function getId() {
		return $this->id;
	}

	public function retrieveData($type, $value) {
		return (is_array($this->data[$type]) && isset($this->data[$type][$value])) ? $this->getClassName() . $this->data[$type][$value] : false;
	}

	public function getClassName() {
		return get_class($this);
	}

	public function getClass($value) {
		return $this->retrieveData('class_name', $value);
	}

	public function getKeyClass() {
		return $this->retrieveData('class_name', 'key');
	}

	public function getValueClass() {
		return $this->retrieveData('class_name', 'value');
	}

	public function getTable($value) {
		return $this->retrieveData('table', $value);
	}

	public function getValuesTable(){
		return $this->retrieveData('table', 'values');
	}

	public function getSearchIndexTable(){
		return $this->retrieveData('table', 'search_index');
	}

	public function setAttribute($ak, $value) {
		if (!is_object($ak)) {
			$ak = PrimeAttributeKey::getByHandle($ak);
		}
		$ak->setAttribute($this, $value);
		$this->reindex();
	}

	public function reindex() {
		$id = $this->getId();
		$attributes = PrimeAttributeKey::getAttributes($id, 'getSearchIndexValue');
		$search_index = $this->getSearchIndexTable();

		$db = Loader::db();
		$db->Execute("DELETE FROM {$search_index} WHERE id = ?", $id);
		$searchable_attributes = array('id' => $id);
		$rows = $db->Execute("SELECT * FROM {$search_index} WHERE id = -1");
		AttributeKey::reindex($search_index, $searchable_attributes, $attributes, $rows);
	}

	public function getAttribute($ak, $displayMode = false) {
		if (!is_object($ak)) {
			$ak = PrimeAttributeKey::getByHandle($ak);
		}
		if (is_object($ak)) {
			$av = $this->getAttributeValueObject($ak);
			if (is_object($av)) {
				return $av->getValue($displayMode);
			}
		}
	}

	public function getAttributeField($ak) {
		if (!is_object($ak)) {
			$ak = PrimeAttributeKey::getByHandle($ak);
		}
		$value = $this->getAttributeValueObject($ak);
		$ak->render('form', $value);
	}

	public function getAttributeValueObject($ak, $createIfNotFound = false) {
		$db = Loader::db();
		$av = false;
		$values = array($this->id(), $ak->getAttributeKeyID());
		$values_table = $this->getValuesTable();
		$query = "SELECT avID FROM {$values_table} WHERE id = ? AND akID = ?";
		$avID = $db->GetOne($query, $values);
		if ($avID > 0) {
			$av = PrimeAttributeValue::getByID($avID);
			if (is_object($av)) {
				$av->setGenesis($this);
				$av->setAttributeKey($ak);
			}
		}

		if ($createIfNotFound) {
			$count = 0;

			// Is this avID in use ?
			if (is_object($av)) {
				$count = $db->GetOne("SELECT count(avID) FROM {$values_table} WHERE avID = ?", $av->getAttributeValueID());
			}

			if ((!is_object($av)) || ($count > 1)) {
				$av = $ak->addAttributeValue();
			}
		}

		return $av;
	}

	public function clearAttribute($ak) {
		$db = Loader::db();
		if (!is_object($ak)) {
			$ak = PrimeAttributeKey::getByHandle($ak);
		}
		$av = $this->getAttributeValueObject($ak);
		if (is_object($av)) {
			$av->delete();
		}
		$this->reindex();
	}

	public function delete() {

		parent::delete();
		$db = Loader::db();
		$values_table = $this->getValuesTable();
		$search_index_table = $this->getSearchIndexTable();
		$id = $this->getId();

		$r = $db->Execute("SELECT avID, akID FROM {$values_table} WHERE id = ?", $id);
		while ($row = $r->FetchRow()) {
			$gak = PrimeAttributeKey::getByID($row['akID']);
			$av = $this->getAttributeValueObject($gak);
			if (is_object($av)) {
				$av->delete();
			}
		}

		return $db->query("DELETE FROM {$search_index_table} WHERE id = ?", $id);
	}

}

class PrimeList extends DatabaseItemList {

	protected $attributeFilters = array();
	protected $itemsPerPage = 10;

	public function __call($method, $arguments) {
		if (substr($method, 0, 8) == 'filterBy') {
			$ak_handle = Loader::helper('text')->uncamelcase(substr($method, 8));
			if (count($arguments) == 2) {
				$this->filterByAttribute($ak_handle, $arguments[0], $arguments[1]);
			} else {
				$this->filterByAttribute($ak_handle, $arguments[0]);
			}
		}
	}

	function __construct() {
		$this->attributeClass = $this->genesis('getKeyClass');		
		$this->setBaseQuery();
	}

	/**
	 * This is a pass-through function that calls our Genesis
	 */
	public function genesis() {
		$args = func_get_args();
		$method = array_shift($args);
		$genesis_name = $this->getRootClass();
		$genesis = new $genesis_name;
		return $genesis->{$method}($args);
	}

	public function getSearchIndexTable() {
		return $this->genesis('getSearchIndexTable');
	}

	/**
	 * Returns the name of the Genesis class (removes List suffix) 
	 * @return [string] Genesis class
	 */
	public function getRootClass() {
		return substr(get_class($this), 0, -4);
	}

	protected function setBaseQuery() {
		$genesis_name = $this->getRootClass();
		$this->setQuery("SELECT g.id FROM {$genesis_name} g ");
	}

	public function get($itemsToGet = 100, $offset = 0) {
		$items = array();
		$this->createQuery();
		$rows = parent::get($itemsToGet, intval($offset));
		foreach($rows as $row) {
			$item = $this->genesis('getByID', $row['id']);
			$items[] = $item;
		}
		return $items;
	}

	public function getTotal(){
		$this->createQuery();
		return parent::getTotal();
	}

	protected function createQuery(){}

}