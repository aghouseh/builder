<?php defined('C5_EXECUTE') or die(_('Access Denied.'));

/***************************************************************************************************
 *	    ____        _ __    __         
 *	   / __ )__  __(_) /___/ /__  _____
 *	  / __  / / / / / / __  / _ \/ ___/
 *	 / /_/ / /_/ / / / /_/ /  __/ /    
 *	/_____/\__,_/_/_/\__,_/\___/_/     
 *	                                   
 * -----------------------------------------------------------------------------------------------
 * 
 * Class BuilderPackage
 * Package controller
 *
 * @package Builder
 * @version 0.1.0
 * @author Andrew Householder <andrew@artesiandesigninc.com>
 */

class BuilderPackage extends Package {

	/**
	 * Concrete5 required properties & methods
	 */
	protected $pkgHandle = 'builder';
	protected $appVersionRequired = '5.6.0';
	protected $pkgVersion = '0.1.0';

	public function getPackageDescription() {
		return t('Extensible custom object creation & management framework for concrete5.');
	}
	
	public function getPackageName() {
		return t('Builder');
	}
	
	/**
	 * Config Keys
	 */
	public $configurable_keys = array();
	
	/**
	 * Package Install
	 * 
	 * @return [void]
	 */
	public function install($options = null) {
		// checks core_commerce existence/version
		if ($this->hasRequiredDependencies()) {
			parent::install();
			//$this->_installConfig($options);
			$this->_configure();
		} else {
			throw new exception(t('This package requires the Concrete5 eCommerce package version 1.5 or greater.'));
			exit;
		}
	}

	/**
	 * Package Upgrade method
	 * 
	 * @return [void]
	 */
	public function upgrade() {
		$this->_configure();
		parent::upgrade();
	}

	/**
	 * Package Uninstall
	 *
	 * @return [void]
	 */
	public function uninstall($options = null) {
		// for proper cleanup, we need to run a few things before uninstall
		parent::uninstall();
		$this->_cleanup($options);
	}

	/**
	 * on_start method runs at the start of the app to register our custom event handler
	 * 
	 * @return [void]
	 */
	public function on_start() {
		// we setup our base autoload data
		$autoloads = array(
			'Prime'             => array('library', 'attribute/model/prime', 'builder'),
			'PrimeList'         => array('library', 'attribute/model/prime', 'builder'),
			'PrimeAttributeKey' => array('library', 'attribute/category/prime', 'builder'),
			'Creator'           => array('library', 'creator', 'builder'),
			'Genesis'           => array('model', 'genesis', 'builder')
		);
		Loader::model('genesis', 'builder');
		$geneses = Genesis::getAll();
		foreach ((array) $geneses as $genesis) {
			$autoloads[$genesis->getName()] = array('model', $genesis->getHandle() . '/model', 'builder');
		}
		Loader::registerAutoload($autoloads);
	}

	/**
	 * Unified method to save our config data given an array (typically $_POST/$_REQUEST)
	 * 
	 * @return [void]
	 */
	public function setConfigSettings($data) {
		if (is_array($data)) {
			$pkg = Package::getByHandle($this->pkgHandle);
			foreach ($this->configurable_keys as $key) {
				$pkg->saveConfig($key, $data[$key]);
			}
		}
	}

	/**
	 * Retrieve our current settings array
	 * 
	 * @return [Array]
	 */
	public function getConfigSettings() {
		$pkg = Package::getByHandle($this->pkgHandle);
		$config_settings = array();
		foreach ($this->configurable_keys as $key) {
			$config_settings[$key] = $pkg->config($key);
		}
		return $config_settings;
	}

	/**
	 * Our configure method that runs on install/upgrade to support our custom installations
	 * 
	 * @return [void]
	 */
	private function _configure() {
		//$this->_verifyAttributes(); // attributes
		$this->_verifySinglePages(); // dashboard pages
	}

	/**
	 * This method checks whether our required attribute instance is installed.
	 * 
	 * @return [void]
	 */
	// private function _verifyAttributes() {
	// 	$pkg = Package::getByHandle($this->pkgHandle);
	// 	Loader::model('attribute/categories/core_commerce_product', 'core_commerce');
	// 	if (!$product_additional_message = CoreCommerceProductAttributeKey::getByHandle($this->instructions_attribute_handle)) {
	// 		$product_additional_message = CoreCommerceProductAttributeKey::add(
	// 			'textarea', // attribute type 
	// 			array( // data object
	// 				'akHandle'              => $this->instructions_attribute_handle, // attribute handle
	// 				'akName'                => t('Additional Message On Order'), // attribute name
	// 				'akTextareaDisplayMode' => 'rich_text', // set the default display mode
	// 				'akIsSearchable'        => false, // not searchable 
	// 				'akIsSearchableIndexed' => true // but do index the data
	// 			),
	// 			$pkg // pass our package object as well for proper association for uninstall
	// 		);
	// 	}
	// }

	/**
	 * Run on package uninstall to cleanup any lingering data
	 *
	 * @return [void]
	 */
	private function _cleanup($options) {
		//if ($options['remove_data']) {
			$this->_removeDB(); // remove custom DB
		//}
	}

	/**
	 * Manually remove DB tables
	 * @return [void]
	 */
	private function _removeDB() {
		$db = Loader::db();
		$sql = "DROP TABLE IF EXISTS Geneses";
		$db->Execute($sql);
	}


	/**
	 * Verifies installation of our dashboard single pages
	 * 
	 * @return [void]
	 */
	private function _verifySinglePages() {
		$this->_installSinglePage('dashboard/builder', array(
			'cName'        => t('Builder'),
			'cDescription' => t('Manage your Geneses.')
		));
		$this->_installSinglePage('dashboard/builder/list', array(
			'cName'        => t('List'),
			'cDescription' => t('Show a listing of your currently generated objects.')
		), 'icon-list');
	}

	/**
	 * Installs a single page with a path and data array
	 * @param  [type] $page_path [description]
	 * @param  [type] $data      [description]
	 * @return [type]            [description]
	 */
	private function _installSinglePage($page_path, $data, $icon) {
		$pkg = Package::getByHandle($this->pkgHandle);
		$single_page = Page::getByPath($page_path);
		if (!$single_page instanceof Page || $single_page->isError()) {
			$single_page = SinglePage::add($page_path, $pkg);
		}
		$single_page->update($data);
		if ($icon) { $single_page->setAttribute('icon_dashboard', $icon); }
		return $single_page;
	}

	/**
	 * Install our attribute types and associate them to the proper categories
	 * 
	 * @return [void]
	 */
	public function _installAttributeTypes() {

		$genesis = AttributeType::getByHandle('genesis');
		if (!is_object($genesis) || !intval($genesis->getAttributeTypeID())) {
			$pkg = Package::getByHandle($this->pkgHandle);
			$genesis = AttributeType::add('genesis', t('Genesis (Builder Object)'), $pkg);
		}

		foreach ($this->categories_for_type as $category) {
			$akc = AttributeKeyCategory::getByHandle($category);
			if (!$this->_hasAttributeTypeAssociation($akc, $genesis)) {
				$akc->associateAttributeKeyType($genesis);
			}
		}

	}


	/**
	 * Setup our default configuration options within the package
	 * 
	 * @return [void]
	 */
	// private function _installConfig($options) {
	// 	$this->setConfigSettings($options);
	// 	Package::getByHandle($this->pkgHandle)->saveConfig('CORE_COMMERCE_SPECIAL_INSTRUCTIONS_INSTRUCTIONS_ATTRIBUTE_HANDLE', $this->instructions_attribute_handle);
	// }

	/**
	 * Package dependecies, if any.
	 * @return [Boolean]
	 */
	public function hasRequiredDependencies() {
		return true;
	}

}